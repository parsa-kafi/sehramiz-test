$(function () {
    function startTime() {
        $.ajax({
            url: window.BASE_URL + '/dashboard/admin-ui',
            method: 'GET',
            dataType: "json",
            cache: false

        }).done(function (data) {
            if (!data.status) {
                $.notify({message: window.MSGS.eanu}, {delay: 2000, type: "danger"});
                return false;
            }
            $('#head-time').text(data.time);
            $('.navbar-top-links .notification').remove();
            $('#header-before-notifications').after(data.notifications);

        }).fail(function (data) {
            $.notify({message: window.MSGS.eanu}, {delay: 2000, type: "danger"});
            return false;

        }).complete(function (data) {
            t = setTimeout(function () {
                startTime()
            }, 20000);
        });
    }

    startTime();
});

$(function () {
    $('.table-order tbody').sortable({
        handle: '.order-handler',
        helper: function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });

            return $helper;
        },
        update: function (e, ui) {
            var data = {orders: []};
            $(e.target).find('tr').each(function (index, tr) {
                data.orders.push($(tr).attr('data-id'));
            });
            $.ajax({
                url: $(e.target).closest('table').attr('data-order-url'),
                data: data,
                method: 'GET',
                dataType: "json",
                cache: false
            }).done(function (data) {
                if (!data.status) {
                    $.notify({message: window.MSGS.eanu}, {delay: 2000, type: "danger"});
                    return false;
                }

                $.notify({message: window.MSGS.updated}, {delay: 2000, type: "success"});
            }).fail(function (data) {
                $.notify({message: window.MSGS.eanu}, {delay: 2000, type: "danger"});
                return false;
            });
        }
    });

    $(document).on('change', '.pools-operators-filter', function () {
        $('.operators-filter option').remove();
        var self = $(this);
        $.ajax({
            url: window.BASE_URL + '/operator/lists?pool_id=' + $(this).val(),
            method: 'GET',
            dataType: "json",
            cache: false

        }).done(function (data) {
            if (!data.status) {
                $.notify({message: window.MSGS.eanu}, {delay: 2000, type: "danger"});
                return false;
            }
            $('.operators-filter option').remove();

            if (self.hasClass('allow-all'))
                $('.operators-filter').append('<option value="">همه</option>');

            $.each(data.operators, function (index, operators) {
                $('.operators-filter').append('<option value=' + operators.operators_id + '>' + operators.name + '</option>');
            });

        }).fail(function (data) {
            $.notify({message: window.MSGS.eanu}, {delay: 2000, type: "danger"});
            return false;

        });
    });

    $('.add_slider_item').on('click', function (event) {
        event.preventDefault();
        $(".slider_item:last-child").clone().insertAfter(".slider_item:last-child")
            .find('.btn-danger').removeClass('hidden').closest('.slider_item')
            .find('input.image_title').val("").closest('.slider_item')
            .find('input.image_file').val("");
        remove_slider_item();
    });

    function remove_slider_item() {
        $('.remove_slider_item').on('click', function (event) {
            event.preventDefault();
            $(this).closest('.slider_item').remove();
        });
    }
});

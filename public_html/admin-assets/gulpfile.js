var gulp         = require('gulp')
,   less         = require('gulp-less')
,   watch        = require('gulp-watch')
,   uglify       = require('gulp-uglify')
,   rename       = require('gulp-rename')
,   plumber      = require('gulp-plumber')
,   sourcemaps   = require('gulp-sourcemaps')
,   minifyCss    = require('gulp-minify-css')
,   autoprefixer = require('gulp-autoprefixer')
,   gulpbower    = require('./gulpbower.js');

gulp.task('default', ['watch']);

gulp.task('watch', function() {
    var watcherJs = gulp.watch(['js/*.js'], ['generate-js']);
    watcherJs.on('change', function(event) {
        console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });

    var watcherJsPages = gulp.watch(['js/pages/*.js'], ['generate-js-pages']);
    watcherJsPages.on('change', function(event) {
        console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });

    var watcherLess = gulp.watch(['less/*.less'], ['generate-less']);
    watcherLess.on('change', function(event) {
        console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });

    var watcherCss = gulp.watch(['css/*.css'], ['generate-css']);
    watcherCss.on('change', function(event) {
        console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
});

gulp.task('generate-js', function() {
    gulp.src('js/*.js')
        .pipe(plumber())
        .pipe(uglify())
        .pipe(rename(function(path) {
            path.basename += ".min";
            path.extname = ".js"
        }))
        .pipe(gulp.dest('dist/js'));
});

gulp.task('generate-js-pages', function() {
    gulp.src('js/pages/*.js')
        .pipe(plumber())
        .pipe(uglify())
        .pipe(rename(function(path) {
            path.basename += ".min";
            path.extname = ".js"
        }))
        .pipe(gulp.dest('dist/js/pages'));
});

gulp.task('generate-less', function() {
    gulp.src('less/main.less')
        .pipe(plumber())
        .pipe(less())
        .pipe(autoprefixer())
        .pipe(minifyCss({keepSpecialComments: 0}))
        .pipe(rename(function(path) {
            path.basename += ".min";
            path.extname = ".css"
        }))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('generate-css', function() {
    gulp.src('css/*.css')
        .pipe(plumber())
        .pipe(autoprefixer())
        .pipe(minifyCss({keepSpecialComments: 0}))
        .pipe(rename(function(path) {
            path.basename += ".min";
            path.extname = ".css"
        }))
        .pipe(gulp.dest('dist/css'));
});

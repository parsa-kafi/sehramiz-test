<?php

namespace Sehramiz\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Sehramiz\Classes\EloquentPriceTrait;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Partner extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, EloquentPriceTrait;

    protected $table = 'partners';

    protected $primaryKey = 'partner_id';

    protected $hidden = ['password', 'remember_token', 'api_key'];

    public function getFullNameAttribute()
    {
        return "$this->name $this->last_name";
    }

    public function ipRestrict()
    {
        return $this->hasMany('Sehramiz\Models\PartnerIpRestrict', 'partner_id', 'partner_id');
    }
}

<?php

namespace Sehramiz\Models;

use Illuminate\Database\Eloquent\Model;

class Festival extends Model
{
    protected $table = 'festivals';

    protected $primaryKey = 'festival_id';

    public $timestamps = true;

    public function partners()
    {
        return $this->belongsToMany('Sehramiz\Models\Partner', 'festivals_partners', 'festival_id', 'partner_id');
    }
}

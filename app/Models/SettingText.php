<?php

namespace Sehramiz\Models;

use Illuminate\Database\Eloquent\Model;

class SettingText extends Model
{
    protected $table = 'settings_text';

    protected $primaryKey = 'setting_text_id';

    public $timestamps = false;
}

<?php

namespace Sehramiz\Models;

use Illuminate\Database\Eloquent\Model;
use Sehramiz\Classes\EloquentPriceTrait;
use Sehramiz\Classes\EloquentPaymentTrait;
use Sehramiz\Classes\EloquentTimestampJalaliTrait;

class PartnerCreditEvent extends Model
{
    use EloquentPriceTrait
    ,   EloquentPaymentTrait
    ,   EloquentTimestampJalaliTrait;

    protected $table = 'partners_credit_events';

    protected $primaryKey = 'partner_credit_event_id';

    public $timestamps = true;

    public function getCodeMessageAttribute()
    {
        return trans('general-words.partner_credit_event_'.$this->code);
    }
}

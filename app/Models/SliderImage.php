<?php

namespace Sehramiz\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Sehramiz\Classes\EloquentPriceTrait;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class SliderImage extends Model implements AuthenticatableContract, CanResetPasswordContract
{
	use Authenticatable, CanResetPassword, EloquentPriceTrait;

	protected $table = 'slider_image';
	protected $visible = ['title', 'name'];
	protected $primaryKey = 'id';

	public function slider()
	{
		return $this->belongsTo('Sehramiz\Models\Slider', 'slider_id', 'id');
	}
}

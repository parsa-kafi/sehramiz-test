<?php

namespace Sehramiz\Http\Controllers\Auth;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Sehramiz\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;

class AdminAuthController extends Controller
{
    use ThrottlesLogins;

    protected $maxLoginAttempts = 5;

    public function __construct()
	{
		$this->auth = Auth::guard('admin');

		$this->middleware('guest:admin', ['except' => 'getLogout']);
	}

    public function getLogin()
    {
        return view('admin.auth.login');
    }

    public function postLogin(Request $request)
    {
        $this->incrementLoginAttempts($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        if ($this->auth->attempt($request->only('username', 'password'), $request->has('remember'))) {

            $this->clearLoginAttempts($request);
            return redirect(action('Admin\DashboardController@getIndex'));
        }

        return redirect($this->loginPath())->withInput()->withErrors(['username' => Lang::get('auth.admin-failed')]);
    }

    public function getLogout()
    {
        $this->auth->logout();

        return redirect('/');
    }

    protected function loginUsername()
    {
        return 'username';
    }

    protected function getLockoutErrorMessage($seconds)
    {
        return Lang::get('auth.admin-throttle', ['seconds' => $seconds]);
    }

    protected function loginPath()
    {
        return action('Auth\AdminAuthController@getLogin');
    }
}

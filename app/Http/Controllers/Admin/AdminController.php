<?php

namespace Sehramiz\Http\Controllers\Admin;

use C;
use Auth;
use Sehramiz\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Sehramiz\Resources\Admin\AdminValidator;
use Sehramiz\Resources\Admin\AdminRepository;

class AdminController extends Controller
{
    protected $category = 'admin';

    public function __construct(Request $request, AdminRepository $adminRepository, AdminValidator $adminValidator)
    {
        $this->adminRepository = $adminRepository;
        $this->adminValidator = $adminValidator;
        $this->request = $request;
    }

    public function getIndex()
    {
        return $this->fire('admin.admin.index', array(
            'admins' => Admin::all(),
        ));
    }

    public function getCreate()
    {
        return $this->fire('admin.admin.create');
    }

    public function postCreate()
    {
        if (($errors = $this->adminValidator->validate()) !== true)
            return redirect()->back()->withInput()->withErrors($errors);

        try {
            $admin = $this->adminRepository->newAdmin();

            $this->adminRepository->transaction($admin);
            $this->adminRepository->setAdminAvatar($admin);

        } catch(\Exception $e) {
            return redirect(action('Admin\AdminController@getIndex'))
                ->with('f-message', ['t' => 'danger', 'm' => Lang::get('admin-messages.error_exception', array('message' => $e->getMessage()))]);
        }

        return redirect(action('Admin\AdminController@getIndex'))
            ->with('f-message', ['t' => 'success', 'm' => Lang::get('admin-messages.create_successfully')]);
    }

    public function getUpdate($adminId = null)
    {
        try {
            $admin = Admin::findOrFail($adminId);

        } catch(\Exception $e) {
            return redirect(action('Admin\AdminController@getIndex'))
                ->with('f-message', ['t' => 'danger', 'm' => Lang::get('admin-messages.error_exception', array('message' => $e->getMessage()))]);
        }

        return $this->fire('admin.admin.edit', array(
            'admin' => $admin,
        ));
    }

    public function postUpdate()
    {
        $adminId = $this->request->get('admin_id');

        if (($errors = $this->adminValidator->validate(true)) !== true)
            return redirect()->back()->withInput()->withErrors($errors);

        try {
            $admin = $this->adminRepository->updateAdmin($adminId);

            $this->adminRepository->transaction($admin);
            $this->adminRepository->setAdminAvatar($admin);

        } catch(\Exception $e) {
            return redirect(action('Admin\AdminController@getIndex'))
                ->with('f-message', ['t' => 'danger', 'm' => Lang::get('admin-messages.error_exception', array('message' => $e->getMessage()))]);
        }

        return redirect(action('Admin\AdminController@getIndex'))
            ->with('f-message', ['t' => 'success', 'm' => Lang::get('admin-messages.update_successfully')]);
    }

    public function getDestroy($adminId = null)
    {
        try {
            $admin = Admin::findOrFail($adminId);
            if ($admin->admin_id == Auth::guard('admin')->id())
                throw new \Exception(Lang::get('admin-messages.admin-delete-itsefl'));

            $this->adminRepository->deleteAvatar($adminId, $admin->avatar);
            $admin->delete();
        } catch(\Exception $e) {
            return redirect(action('Admin\AdminController@getIndex'))
                ->with('f-message', ['t' => 'danger', 'm' => Lang::get('admin-messages.error_exception', array('message' => $e->getMessage()))]);
        }

        return redirect(action('Admin\AdminController@getIndex'))
            ->with('f-message', ['t' => 'success', 'm' => Lang::get('admin-messages.delete_successfully')]);
    }
}

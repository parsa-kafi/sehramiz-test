<?php

namespace Sehramiz\Http\Controllers\Admin;

use DB;
use Auth;
use Illuminate\Http\Request;
use Sehramiz\Models\Partner;
use Sehramiz\Models\Festival;
use Sehramiz\Models\Slider;
use Sehramiz\Models\SliderImage;
use Illuminate\Support\Facades\Lang;
use Sehramiz\Resources\Slider\SliderValidator;
use Sehramiz\Resources\Slider\SliderRepository;

class SliderController extends Controller {
	protected $category = 'slider';
	protected $slider_repository;
	protected $slider_validator;
	protected $request;

	public function __construct( Request $request, SliderRepository $slider_repository, SliderValidator $slider_validator ) {
		$this->slider_repository = $slider_repository;
		$this->slider_validator  = $slider_validator;
		$this->request           = $request;
	}

	public function getIndex() {
		return $this->fire( 'admin.slider.index', array(
			'sliders' => Slider::all()
		) );
	}

	public function getCreate() {
		return $this->fire( 'admin.slider.create' );
	}

	public function postCreate() {
		if ( ( $errors = $this->slider_validator->validate() ) !== true ) {
			return redirect()->back()->withInput()->withErrors( $errors );
		}

		try {
			$slider = $this->slider_repository->newSlider();
			$images = $this->slider_repository->newImages( $slider->id );

			$this->slider_repository->transaction( $slider, $images );

		} catch ( \Exception $e ) {
			return redirect( action( 'Admin\SliderController@getIndex' ) )
				->with( 'f-message', [
					't' => 'danger',
					'm' => Lang::get( 'admin-messages.error_exception', array( 'message' => $e->getMessage() ) )
				] );
		}

		return redirect( action( 'Admin\SliderController@getIndex' ) )
			->with( 'f-message', [ 't' => 'success', 'm' => Lang::get( 'admin-messages.create_successfully' ) ] );
	}

	public function getUpdate( $sliderId = null ) {
		try {
			$slider = Slider::findOrFail( $sliderId );

		} catch ( \Exception $e ) {
			return redirect( action( 'Admin\SliderController@getIndex' ) )
				->with( 'f-message', [
					't' => 'danger',
					'm' => Lang::get( 'admin-messages.error_exception', array( 'message' => $e->getMessage() ) )
				] );
		}

		return $this->fire( 'admin.slider.edit', array(
			'slider'       => $slider,
			'slider_image' => SliderImage::where( 'slider_id', $slider->id )->get(),
		) );
	}

	public function postUpdate() {
		$slider_id = $this->request->get( 'slider_id' );

		if ( ( $errors = $this->slider_validator->validate( true ) ) !== true ) {
			return redirect()->back()->withInput()->withErrors( $errors );
		}

		try {
			$slider = $this->slider_repository->update( $slider_id );
			$images = $this->slider_repository->newImages( $slider_id );
			$this->slider_repository->transaction( $slider, $images );

		} catch ( \Exception $e ) {
			return redirect( action( 'Admin\SliderController@getIndex' ) )
				->with( 'f-message', [
					't' => 'danger',
					'm' => Lang::get( 'admin-messages.error_exception', array( 'message' => $e->getMessage() ) )
				] );
		}

		return redirect( action( 'Admin\SliderController@getIndex' ) )
			->with( 'f-message', [ 't' => 'success', 'm' => Lang::get( 'admin-messages.update_successfully' ) ] );
	}

	public function getDestroy( $sliderId = null ) {
		try {
			$slider = Slider::findOrFail( $sliderId );
			$images = SliderImage::where( 'slider_id', $sliderId )->get();
			foreach ( $images as $image ) {
				$image = SliderImage::findOrFail( $image->id );
				unlink( public_path( 'uploads/' . $image->name ) );
				$image->delete();
			}
			$slider->delete();
		} catch ( \Exception $e ) {
			return redirect( action( 'Admin\SliderController@getIndex' ) )
				->with( 'f-message', [
					't' => 'danger',
					'm' => Lang::get( 'admin-messages.error_exception', array( 'message' => $e->getMessage() ) )
				] );
		}

		return redirect( action( 'Admin\SliderController@getIndex' ) )
			->with( 'f-message', [ 't' => 'success', 'm' => Lang::get( 'admin-messages.delete_successfully' ) ] );
	}

	public function getDestroyImage( $slider = null, $imageId = null ) {
		try {
			$image = SliderImage::findOrFail( $imageId );
			unlink( public_path( 'uploads/' . $image->name ) );
			$image->delete();

		} catch ( \Exception $e ) {
			return redirect( action( 'Admin\SliderController@getUpdate', $slider ) )
				->with( 'f-message', [
					't' => 'danger',
					'm' => Lang::get( 'admin-messages.error_exception', array( 'message' => $e->getMessage() ) )
				] );
		}

		return redirect( action( 'Admin\SliderController@getUpdate', $slider ) )
			->with( 'f-message', [ 't' => 'success', 'm' => Lang::get( 'admin-messages.delete_successfully' ) ] );
	}
}

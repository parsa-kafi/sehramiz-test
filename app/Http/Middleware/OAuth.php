<?php

namespace Sehramiz\Http\Middleware;

use C;
use DB;
use Date;
use Closure;
use Sehramiz\Models\OAuthAccessToken;
use Illuminate\Support\Facades\Auth;

class OAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $scope)
    {
        $accessToken = $request->get('access_token');

        $accessToken = OAuthAccessToken::where('oauth_client_id', $request->get('oauth_client_id'))
            ->where('access_token', $accessToken)
            ->where('scope', $scope)
            ->first();

        if (is_null($accessToken))
            return response(['status' => 0, 'oauth_error' => 'invalid_access_token'], 400);

        $expireIn = new Date($accessToken->expire_date);
        $now = Date::now();

        if ($now->gt($expireIn))
            return response(['status' => 0, 'oauth_error' => 'expired_access_token'], 400);

        Auth::shouldUse($scope);
        Auth::loginUsingId($accessToken->related_id);

        return $next($request);
    }
}

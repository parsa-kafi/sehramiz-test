<?php

namespace Sehramiz\Classes;

use C;
use Illuminate\Support\Facades\Lang;

/**
 * For models have price or credit field
 */
trait EloquentPriceTrait
{
    /**
     * Show price in admin panel
     *
     * @return string
     */
    public function getPriceAdminAttribute()
    {
        return price($this->price)->sep()->fa()->format();
    }

    /**
     * Show credit in admin panel
     *
     * @return string
     */
    public function getCreditAdminAttribute()
    {
        return price($this->credit)->sep()->fa()->format();
    }
}

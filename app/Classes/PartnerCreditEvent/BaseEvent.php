<?php

namespace Sehramiz\Classes\PartnerCreditEvent;

use Sehramiz\Models\Partner;
use Sehramiz\Models\PartnerCreditEvent;

class BaseEvent
{
    public function __construct(Partner $partner, $eventCode, PartnerCreditEvent $event)
    {
        $this->partner = $partner;
        $this->eventCode = $eventCode;
        $this->event = $event;

        $this->loadRelated();
    }

    public function eventCode()
    {
        return $this->eventCode;
    }

    public function description()
    {
        return $this->event->description;
    }
}

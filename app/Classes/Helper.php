<?php

namespace Sehramiz\Classes;

use C;

class Helper
{
    /**
     * Mobile identofier, all converts to 09[0-9]{9} format
     *
     * @param string $mobile
     * @return string|null
     */
    public static function mobileIdentifier($mobile)
    {
        $m1 = '/\A09[0-9]{9}/';
        $m2 = '/\A\+989[0-9]{9}/';
        $m3 = '/\A\+9809[0-9]{9}/';
        $m4 = '/\A00989[0-9]{9}/';
        $m5 = '/\A9[0-9]{9}/';

        preg_match($m1, $mobile, $output);
        if (!empty($output)) {
            return $mobile;
        }

        preg_match($m2, $mobile, $output);
        if (!empty($output)) {
            return '0'.substr($mobile, 3);
        }

        preg_match($m3, $mobile, $output);
        if (!empty($output)) {
            return substr($mobile, 3);
        }

        preg_match($m4, $mobile, $output);
        if (!empty($output)) {
            return '0'.substr($mobile, 4);
        }

        preg_match($m5, $mobile, $output);
        if (!empty($output)) {
            return '0'.$mobile;
        }

        return null;
    }

    public static function partnerCreditEventCodesArray()
    {
        return [
            C::PC_E_INCREASE_ADMIN,
            C::PC_E_DECREASE_ADMIN,
            C::PC_E_DECREASE_GENERATE_CODE,
            C::PC_E_SEND_SMS,
        ];
    }

    /**
     * @return array
     */
    public static function partnerCreditEventCodesList()
    {
        $array = [];
        foreach (self::partnerCreditEventCodesArray() as $pc) {
            $array[$pc] = trans('general-words.partner_credit_event_'.$pc);
        }

        return $array;
    }

    public static function paymentsArray()
    {
        return [
            C::NOT_PAID,
            C::PAID
        ];
    }

    /**
     * Return payments list for show in select input
     *
     * @return array
     */
    public static function paymentsList()
    {
        $payments = [];

        $payments[C::PAID] = trans('general-words.paid');
        $payments[C::NOT_PAID] = trans('general-words.not_paid');

        return $payments;
    }

    /**
     * Return genders list for show in select input
     *
     * @param bool $either
     * @param bool $plural
     * @return array
     */
    public static function gendersList($either = true, $plural = true)
    {
        $genders = [];

        if ($either) {
            $genders[C::GENDER_EITHER] = trans('general-words.gender_either');
        }

        $genders[C::MALE] = $plural ? trans('general-words.males') : trans('general-words.male');
        $genders[C::FEMALE] = $plural ? trans('general-words.females') : trans('general-words.female');

        return $genders;
    }
}

<?php namespace Sehramiz\Resources;

use Illuminate\Http\Request;

class Repository
{
    /**
     * Main model in repository
     */
    public $model;

    /**
     * Transaction
     */
    public function transaction($model)
    {
        try {
            $model->save();
            $this->commit();
            return $model;
        }
        catch(\PDOException $e) {
            $this->rollback();
            throw $e;
        }
    }

    /**
     * Start transaction
     */
    public function beginTransaction()
    {
        $this->db->beginTransaction();
    }

    /**
     * Commit transaction
     */
    public function commit()
    {
        $this->db->commit();
    }

    /**
     * RollBack transaction
     */
    public function rollback()
    {
        $this->db->rollBack();
    }
}

<?php namespace Sehramiz\Resources;

use Illuminate\Http\Request;
use Validator as V;

class Validator
{
    /**
     * Rules
     *
     * @var array
     */
    protected $rules = array();

    /**
     * Update rules
     *
     * @var array
     */
    protected $rulesUpdate = array();

    /**
     * Custome messages for validation
     *
     * @var array
     */
    protected $customMessages = array();

    /**
     * When forUpdate is true merge rules with updateRules or not
     *
     * @var bool
     */
    protected $mergeRules = true;

    /**
     * Initialize class
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Validate rules
     *
     * @param bool $forUpdate
     *
     * @return
     */
    public function validate($forUpdate = false, $beforeValidate = null, $inputs = null)
    {
        if (!is_null($beforeValidate))
            $this->{$beforeValidate}();

        $rules = $this->getRules($forUpdate);
        $inputs = $inputs == null ? $this->request->all() : $inputs;
        $v = V::make($inputs, $rules, $this->customMessages);
        if ($v->fails()) {
            return $v->messages();
        }

        return true;
    }

    /**
     * Get rules for validate
     *
     * @param bool $forUpdate
     *
     * @return array
     */
    public function getRules($forUpdate)
    {
        if (!$forUpdate) return $this->rules;

        if ($this->mergeRules) {
            return array_merge($this->rules, $this->rulesUpdate);
        } else {
            return $this->rulesUpdate;
        }
    }

    /**
     * Implode user input
     */
    protected function implode($input)
    {
        $input = $this->request->get($input);
        if (is_array($input)) {
            return implode(',', $input);
        }

        return '';
    }
}

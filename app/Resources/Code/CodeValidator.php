<?php

namespace Sehramiz\Resources\Code;

use Auth;
use Illuminate\Http\Request;
use Sehramiz\Resources\Validator;
use Sehramiz\Resources\ValidatorInterface;

class CodeValidator extends Validator
{
    public function __construct(Request $request)
    {
        $this->mergeRules = true;

        $this->rules = array();

        $this->rulesUpdate = array();

        call_user_func_array('parent::__construct', func_get_args());
    }
}

<?php

namespace Sehramiz\Resources\Slider;

use Auth;
use Illuminate\Http\Request;
use Sehramiz\Resources\Validator;
use Sehramiz\Resources\ValidatorInterface;

class SliderValidator extends Validator
{
    public function __construct(Request $request)
    {
        $this->mergeRules = false;

        $this->rules = array(
            'name' => 'required'
        );

        call_user_func_array('parent::__construct', func_get_args());
    }
}

<?php namespace Sehramiz\Resources\Admin;

use Sehramiz\Resources\ValidatorInterface;
use Sehramiz\Resources\Validator;
use Illuminate\Http\Request;

class AdminValidator extends Validator
{
    public function __construct(Request $request)
    {
        $this->mergeRules = true;

        $this->rules = array(
            'username' => 'required|unique:admins,username',
            'email' => 'required|unique:admins,email',
            'name' => 'required|string',
            'mobile' => 'mobile|unique:admins,mobile',
            'password' => 'required|string|confirmed|min:8',
            'avatar' => 'image'
        );

        $this->rulesUpdate = array(
            'admin_id' => 'required|exists:admins,admin_id',
            'username' => 'required|unique:admins,username,'.$request->get('admin_id').',admin_id',
            'email' => 'required|unique:admins,email,'.$request->get('admin_id').',admin_id',
            'mobile' => 'mobile|unique:admins,mobile,'.$request->get('admin_id').',admin_id',
            'password' => 'string|confirmed|min:8',
        );

        call_user_func_array('parent::__construct', func_get_args());
    }
}

<?php

use Illuminate\Database\Seeder;

class PartnerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $partners = [
            array(
                'partner_id' => 1,
                'email' => 'admin@partner.com',
                'username' => 'admin',
                'api_key' => '75bc48ddc7c847c5831f56a767',
                'name' => 'سایت همکار',
                'mobile' => '09368353090',
                'password' => bcrypt('123456'),
                'status' => 1
            )
        ];
        DB::table('partners')->insert($partners);
    }
}

@extends('pool-admin.layout.main')

@section('title', 'معین حساب‌ها')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">حسابداری</h1>
        <div class="panel panel-default">
            <div class="panel-heading">تمامی فاکتورها</div>
            <div class="panel-body">
                @if (Session::has('f-message'))
                    <div class="alert alert-{{Session::get('f-message')['t']}}">
                        {!! Session::get('f-message')['m'] !!}
                    </div>
                @endif
                <div class="text-left">
                    <?php
                        $requests = Request::all();
                        $requests['print'] = true;
                    ?>
                    <a href="{{action('PoolAdmin\LedgerController@getAllFactor')}}?{{http_build_query($requests)}}" class="btn btn-primary" target="_blank">
                        <i class="fa fa-print"></i> چاپ
                    </a>
                </div>
                <br>
                <h2 class="text-center">تمامی فاکتورها</h2>
                <h3 class="text-center">از تاریخ {{to_j($from, false)}} تا تاریخ {{to_j($to, false)}}</h3>
                <br>
                @foreach ($ledgers as $ledger)
                <h3 class="text-center">فاکتور</h3>
                <table class="table clean">
                    <tr>
                        <td>طرف حساب: {{$pool->name}}</td>
                        <td class="text-left">تاریخ فاکتور: <span dir="ltr">{{@to_jalali($ledger->date, 'Y/m/d H:i:s l')}}</span></td>
                    </tr>
                    <tr>
                        <td>شماره فاکتور: {{@tr_num($ledger->factor_id, 'fa')}}</td>
                        <td class="text-left">ساعت فاکتور: {{@to_time($ledger->date, false, true)}}</td>
                    </tr>
                    <tr>
                        <td>نام سانس: {{$ledger->reservation->saens_name}}</td>
                        <td class="text-left">جنسیت: {{Helper::printGender($ledger->reservation->gender)}}</td>
                    </tr>
                </table>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>ردیف</th>
                            <th>عنوان</th>
                            <th>تعداد</th>
                            <th>قیمت</th>
                            <th>پس از تخفیف</th>
                            <th>جمع کل</th>
                        </tr>
                    </thead>
                    <?php $i = 0 ?>
                    <tbody>
                        @if ($ledger->reservation->count_adult != '0')
                        <tr>
                            <td class="en">{{++$i}}</td>
                            <td>بزرگسال</td>
                            <td class="en">{{$ledger->reservation->count_adult}}</td>
                            <td><span class="en">{{price($ledger->reservation->price_market_adult)->sep()}}</span> ریال</td>
                            <td><span class="en">{{price($ledger->reservation->price_buy_adult)->sep()}}</span> ریال</td>
                            <td><span class="en">{{price($ledger->reservation->count_adult * $ledger->reservation->price_buy_adult)->sep()}}</span> ریال</td>
                        </tr>
                        @endif
                        @if ($ledger->reservation->count_child != '0')
                        <tr>
                            <td class="en">{{++$i}}</td>
                            <td>خردسال</td>
                            <td class="en">{{$ledger->reservation->count_child}}</td>
                            <td><span class="en">{{price($ledger->reservation->price_market_child)->sep()}}</span> ریال</td>
                            <td><span class="en">{{price($ledger->reservation->price_buy_child)->sep()}}</span> ریال</td>
                            <td><span class="en">{{price($ledger->reservation->count_child * $ledger->reservation->price_buy_child)->sep()}}</span> ریال</td>
                        </tr>
                        @endif
                        <tr>
                            <td class="en">{{++$i}}</td>
                            <td colspan="3"></td>
                            <td class="active">جمع کل:</td>
                            <td class="success"><span class="en">{{price($ledger->reservation->pay_to_pool)->sep()}}</span> ریال</td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                @endforeach
            </div>
        </div>
    </div>
</div>
@stop

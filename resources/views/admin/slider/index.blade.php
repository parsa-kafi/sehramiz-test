@extends('admin.layout.main')

@section('title', 'لیست اسلایدرها')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">اسلایدرها</h1>
        <div class="panel panel-default">
            <div class="panel-heading">لیست اسلایدرها</div>
            <div class="panel-body">
                <p class="text-left"><a class="btn btn-success" href="{{action('Admin\SliderController@getCreate')}}"><i class="fa fa-plus"></i> اسلایدر جدید</a></p>
                @if (Session::has('f-message'))
                    <div class="alert alert-{{Session::get('f-message')['t']}}">
                        {!! Session::get('f-message')['m'] !!}
                    </div>
                @endif

                <table class="table table-bordered table-hover custom-datatable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>عنوان</th>
                            <th>وضعیت</th>
                            <th>عملیات</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($sliders as $slider)
                        <tr>
                            <td>{{$slider->id}}</td>
                            <td>{{$slider->name}}</td>
                            <td><span class="label label-{{checkStatus($slider->status, 'success', 'danger')}}">{{checkStatus($slider->status, 'فعال', 'غیر فعال')}}</span></td>
                            <td>
                                <a href="{{action('Admin\SliderController@getUpdate', $slider->id)}}" class="btn btn-info btn-xs" title="ویرایش"><i class="fa fa-pencil"></i></a>
                                <a href="{{action('Admin\SliderController@getDestroy', $slider->id)}}" class="btn btn-danger btn-xs confirm" title="حذف"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
<script>
    $('.custom-datatable').DataTable( {
        responsive: true,
        language: window.datatableLanguage,
        order: [[ 0, "desc" ]],
    });
</script>
@stop

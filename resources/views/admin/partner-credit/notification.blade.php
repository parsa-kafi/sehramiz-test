@extends('admin.layout.main')

@section('title', 'اعتبارات کاربران')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">اعتبارات کاربران</h1>
        <div class="panel panel-default">
            <div class="panel-heading">اعتبارات کاربران</div>
            <div class="panel-body">
                @if (Session::has('f-message'))
                    <div class="alert alert-{{Session::get('f-message')['t']}}">
                        {!! Session::get('f-message')['m'] !!}
                    </div>
                @endif
                <table class="table table-bordered table-hover custom-datatable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>کاربر</th>
                            <th>نوع عملیات</th>
                            <th>تاریخ</th>
                            <th>مبلغ</th>
                            <th>پرداخت</th>
                            <th>عملیات</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($creditEvents as $event)
                        <tr>
                            <td class="en">{{$event->user_credit_event_id}}</td>
                            <td><a href="{{action('Admin\UserController@getView', $event->user_id)}}">{{$event->user_name}}</a></td>
                            <td>{{trans('general-words.user_credit_event_'.$event->code)}}</td>
                            <td class="ltr text-right">{{to_j($event->created_at)}}</td>
                            <td><span class="en" dir="ltr">{{price($event->price)->sep()}}</span> ریال</td>
                            <td><span class="label label-{{Helper::ifPayment($event->payment)}}">{{Helper::printPayment($event->payment)}}</span></td>
                            <td>
                                <a href="{{action('Admin\UserCreditController@getView', $event->user_credit_event_id)}}" class="btn btn-primary btn-xs" title="مشاهده"><i class="fa fa-eye"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
<script>
    $('.custom-datatable').DataTable( {
        responsive: true,
        language: window.datatableLanguage,
        order: [[ 3, "desc" ]]
    });
</script>
@stop

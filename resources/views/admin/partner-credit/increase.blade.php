@extends('admin.layout.main')

@section('title', 'افزایش اعتبار')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">افزایش اعتبار
            @include('admin.partner.navigation')
        </h1>
        <div class="panel panel-default">
            <div class="panel-heading">افزایش اعتبار برای {{$partner->full_name}}
                <a class="btn btn-default btn-xs pull-left" href="{{action('Admin\PartnerController@getView', $partner->partner_id)}}" title="برگشت"><i class="fa fa-reply"></i></a>
            </div>
            <div class="panel-body">
                <form action="{{action('Admin\PartnerCreditController@postIncrease')}}" method="post">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>مقدار افزایش</label>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                {!! Form::text('credit', '', ['class' => 'form-control en ltr p-sep-ltr']) !!}
                                <div class="input-group-addon">ریال</div>
                            </div>
                            {!! $errors->first('credit', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>توضیحات</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::textarea('description', '', ['class' => 'form-control', 'rows' => 3]) !!}
                            {!! $errors->first('description', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="partner_id" value="{{$partner->partner_id}}">
                        <a class="btn btn-default pull-right" href="{{action('Admin\PartnerController@getView', $partner->partner_id)}}">برگشت</a>
                        <button type="submit" class="btn btn-info pull-left"><i class="fa fa-plus"></i> افزایش</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

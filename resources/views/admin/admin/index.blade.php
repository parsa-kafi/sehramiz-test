@extends('admin.layout.main')

@section('title', 'لیست ادمین‌ها')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">ادمین‌ها</h1>
        <div class="panel panel-default">
            <div class="panel-heading">لیست ادمین‌ها</div>
            <div class="panel-body">
                <p class="text-left"><a class="btn btn-success" href="{{action('Admin\AdminController@getCreate')}}"><i class="fa fa-plus"></i> ادمین جدید</a></p>
                @if (Session::has('f-message'))
                    <div class="alert alert-{{Session::get('f-message')['t']}}">
                        {!! Session::get('f-message')['m'] !!}
                    </div>
                @endif
                
                <table class="table table-bordered table-hover datatable">
                    <thead>
                        <tr>
                            <th>آی‌دی</th>
                            <th>نام کاربری</th>
                            <th>نام و نام خانوادگی</th>
                            <th>ایمیل</th>
                            <th>وضعیت</th>
                            <th>عملیات</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($admins as $admin)
                            <tr>
                                <td class="en">{{$admin->admin_id}}</td>
                                <td class="en">{{$admin->username}}</td>
                                <td>{{"$admin->name $admin->last_name"}}</td>
                                <td class="en">{{$admin->email}}</td>
                                <td><span class="label label-{{checkStatus($admin->status, 'success', 'danger')}}">{{checkStatus($admin->status, 'فعال', 'غیر فعال')}}</span></td>
                                <td>
                                    <a href="{{action('Admin\AdminController@getUpdate', $admin->admin_id)}}" class="btn btn-info btn-xs" title="ویرایش"><i class="fa fa-pencil"></i></a>
                                    <a href="{{action('Admin\AdminController@getDestroy', $admin->admin_id)}}" class="btn btn-danger btn-xs confirm" title="حذف"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop

    @extends('admin.layout.main')

    @section('title', 'ایجاد همکار جدید')

    @section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">همکار‌ها</h1>
            <div class="panel panel-default">
                <div class="panel-heading">ایجاد همکار جدید
                    <a class="btn btn-default btn-xs pull-left" href="{{action('Admin\PartnerController@getIndex')}}" title="برگشت"><i class="fa fa-reply"></i></a>
                </div>
                <div class="panel-body">
                    <form action="{{action('Admin\PartnerController@postCreate')}}" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>نام @required</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('name', '', ['class' => 'form-control', 'required']) !!}
                                {!! $errors->first('name', "<p class='text text-danger'>:message</p>") !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>نام خانوادگی</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('last_name', '', ['class' => 'form-control']) !!}
                                {!! $errors->first('last_name', "<p class='text text-danger'>:message</p>") !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>ایمیل</label>
                            </div>
                            <div class="col-md-5">
                                {!! Form::text('email', '', ['class' => 'form-control en ltr']) !!}
                                {!! $errors->first('email', "<p class='text text-danger'>:message</p>") !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>موبایل</label>
                            </div>
                            <div class="col-md-3">
                                {!! Form::text('mobile', '', ['class' => 'form-control en ltr']) !!}
                                {!! $errors->first('mobile', "<p class='text text-danger'>:message</p>") !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>نام کاربری @required</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('username', '', ['class' => 'form-control en ltr', 'required']) !!}
                                {!! $errors->first('username', "<p class='text text-danger'>:message</p>") !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>رمز عبور @required</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::password('password', ['class' => 'form-control en ltr', 'required']) !!}
                                {!! $errors->first('password', "<p class='text text-danger'>:message</p>") !!}
                            </div>
                            <div class="col-md-12">
                                <p class="text-warning">حداقل طول رمز 8 کاراکتر</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>تایید رمز عبور @required</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::password('password_confirmation', ['class' => 'form-control en ltr', 'required']) !!}
                                {!! $errors->first('password_confirmation', "<p class='text text-danger'>:message</p>") !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>حداقل میزان اعتبار @required</label>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    {!! Form::text('minimum_credit', '0', ['class' => 'form-control en ltr p-sep-ltr', 'required']) !!}
                                    <div class="input-group-addon">ریال</div>
                                </div>
                                {!! $errors->first('minimum_credit', "<p class='text text-danger'>:message</p>") !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <label>آواتار</label>
                            </div>
                            <div class="col-md-5">
                                <input name="avatar" type="file" class="form-control">
                                {!! $errors->first('avatar', "<p class='text text-danger'>:message</p>") !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <label>آی‌پی‌ها</label>
                            </div>
                            <div class="col-md-5">
                                {!! Form::textarea('ip', '', ['class' => 'form-control en ltr']) !!}
                            </div>
                            <div class="col-md-12">
                                <p class="text-warning">هر IP در یک سطر</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>وضعیت</label>
                            </div>
                            <div class="col-md-2">
                                <label>غیر فعال</label>
                                {!! Form::radio('status', '0', true, ['class' => 'icheck-status disable']) !!}
                                <label>فعال</label>
                                {!! Form::radio('status', '1', false, ['class' => 'icheck-status enable']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="col-md-6 text-right">
                                <a href="{{action('Admin\PartnerController@getIndex')}}" class="btn btn-default">برگشت</a>
                            </div>
                            <div class="col-md-6 text-left">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> ایجاد کن</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @stop

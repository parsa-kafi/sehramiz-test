@extends('admin.layout.main')

@section('title', 'لیست همکاران')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">همکاران</h1>
        <div class="panel panel-default">
            <div class="panel-heading">لیست همکاران</div>
            <div class="panel-body">
                <p class="text-left"><a class="btn btn-success" href="{{action('Admin\PartnerController@getCreate')}}"><i class="fa fa-plus"></i> همکار جدید</a></p>
                @if (Session::has('f-message'))
                    <div class="alert alert-{{Session::get('f-message')['t']}}">
                        {!! Session::get('f-message')['m'] !!}
                    </div>
                @endif

                <table class="table table-bordered table-hover custom-datatable">
                    <thead>
                        <tr>
                            <th class="en">#</th>
                            <th>نام</th>
                            <th>وضعیت</th>
                            <th>عملیات</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($partners as $partner)
                        <tr>
                            <td class="en">{{$partner->partner_id}}</td>
                            <td>{{"$partner->name $partner->last_name"}}</td>
                            <td><span class="label label-{{checkStatus($partner->status, 'success', 'danger')}}">{{checkStatus($partner->status, 'فعال', 'غیر فعال')}}</span></td>
                            <td>
                                <a href="{{action('Admin\PartnerController@getView', $partner->partner_id)}}" class="btn btn-primary btn-xs" title="مشاهده"><i class="fa fa-eye"></i></a>
                                <a href="{{action('Admin\PartnerController@getUpdate', $partner->partner_id)}}" class="btn btn-info btn-xs" title="ویرایش"><i class="fa fa-pencil"></i></a>
                                <a href="{{action('Admin\PartnerController@getDestroy', $partner->partner_id)}}" class="btn btn-danger btn-xs confirm" title="حذف"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
<script>
    $('.custom-datatable').DataTable( {
        responsive: true,
        language: window.datatableLanguage,
        order: [[ 0, "desc" ]],
    });
</script>
@stop

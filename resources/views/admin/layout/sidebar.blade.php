@section('sidebar-cg-'.$category) active @stop

<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <div class="sidebar-profile pool-admin">
            <h4 class="title text-center"><b>پنل ادمین</b></h4>
            @if (Auth::guard('admin')->user()->avatar)
            <div class="pull-right image">
                <img src="{{URL::to(Manage::uploadDir('admins').Auth::id().'.'.Auth::guard('admin')->user()->avatar)}}" class="img-circle">
            </div>
            @endif
            <h5 class="name pull-right">{{Auth::guard('admin')->user()->name}}، خوش آمدید</h5>
        </div>
        <ul class="nav" id="side-menu">
            <li>
                <a class="@yield('sidebar-cg-dashboard')" href="{{action('Admin\DashboardController@getIndex')}}"><i class="fa fa-dashboard fa-fw"></i> داشبورد</a>
            </li>
            <li>
                <a class="@yield('sidebar-cg-festival')" href="{{action('Admin\FestivalController@getIndex')}}"><i class="fa fa-gift fa-fw"></i> جشنواره‌‌ها</a>
            </li>
            <li>
                <a class="@yield('sidebar-cg-slider')" href="{{action('Admin\SliderController@getIndex')}}"><i class="fa fa-picture-o fa-fw"></i> اسلایدرها</a>
            </li>
            <li class="@yield('sidebar-cg-partner') @yield('sidebar-cg-user') @yield('sidebar-cg-admin')">
                <a href="javascript:void(0)"><i class="fa fa-users fa-fw"></i> اعضاء<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a class="@yield('sidebar-cg-admin')" href="{{action('Admin\AdminController@getIndex')}}"> ادمین‌ها</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>

<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" target="_blank" href="{{URL::to('/')}}">سیستم مدیریت جشنواره سحرآمیز</a>
</div>

<ul class="nav navbar-top-links navbar-left">
    <li class="dropdown" id="header-before-notifications">
        <a id="head-time" class="ltr">{{Date\Jalali::now()->fa('Y/m/d H:i l')}}</a>
    </li>
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-user">
            <li>
                <a href="{{URL::action('Admin\AdminController@getUpdate', Auth::guard('admin')->id())}}"><i class="fa fa-user fa-fw"></i> پروفایل</a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="{{action('Auth\AdminAuthController@getLogout')}}"><i class="fa fa-sign-out fa-fw"></i> خروج</a>
            </li>
        </ul>
    </li>
</ul>

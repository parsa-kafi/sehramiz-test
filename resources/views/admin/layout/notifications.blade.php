@if (array_sum($notificationsCount['users']) > 0)
<li class="dropdown notification" title="کاربران">
    <a class="dropdown-toggle text-danger" data-toggle="dropdown" href="#" aria-expanded="true">
        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
    </a>
    <ul class="dropdown-menu dropdown-alerts">
        @if ($notificationsCount['users'][C::AN_NEW_RESERVATION] > 0)
        <li>
            <a href="{{URL::action('Admin\ReservationController@getNotification')}}">
                <div>
                    <i class="fa fa-star fa-fw"></i>{{fa($notificationsCount['users'][C::AN_NEW_RESERVATION])}} رزرو جدید
                    <span class="pull-left text-muted small">مشاهده</span>
                </div>
            </a>
        </li>
        <li class="divider"></li>
        @endif

        @if ($notificationsCount['users'][C::AN_NEW_USER] > 0)
        <li>
            <a href="{{URL::action('Admin\UserController@getNotification')}}">
                <div>
                    <i class="fa fa-user fa-fw"></i>{{fa($notificationsCount['users'][C::AN_NEW_USER])}} کاربر جدید
                    <span class="pull-left text-muted small">مشاهده</span>
                </div>
            </a>
        </li>
        <li class="divider"></li>
        @endif

        @if ($notificationsCount['users'][C::AN_NEW_CONTACT] > 0)
        <li>
            <a href="{{URL::action('Admin\ContactController@getNotification')}}">
                <div>
                    <i class="fa fa-phone fa-fw"></i>{{fa($notificationsCount['users'][C::AN_NEW_CONTACT])}} تماس جدید
                    <span class="pull-left text-muted small">مشاهده</span>
                </div>
            </a>
        </li>
        <li class="divider"></li>
        @endif

        @if ($notificationsCount['users'][C::AN_NEW_COMMENT] > 0)
        <li>
            <a href="{{URL::action('Admin\PoolCommentController@getNotification')}}">
                <div>
                    <i class="fa fa-comment fa-fw"></i>{{fa($notificationsCount['users'][C::AN_NEW_COMMENT])}} نظر جدید
                    <span class="pull-left text-muted small">مشاهده</span>
                </div>
            </a>
        </li>
        <li class="divider"></li>
        @endif

        @if ($notificationsCount['users'][C::AN_USER_CREDIT_INCREASE_PAYMENT] > 0)
        <li>
            <a href="{{URL::action('Admin\UserCreditController@getNotification')}}?event_code={{C::AN_USER_CREDIT_INCREASE_PAYMENT}}">
                <div>
                    <i class="fa fa-comment fa-fw"></i>{{fa($notificationsCount['users'][C::AN_USER_CREDIT_INCREASE_PAYMENT])}} {{trans('general-words.user_credit_event_'.C::UC_E_INCREASE_USER_PAYMENT)}}
                    <span class="pull-left text-muted small">مشاهده</span>
                </div>
            </a>
        </li>
        <li class="divider"></li>
        @endif

        @if ($notificationsCount['users'][C::AN_USER_CREDIT_INCREASE_RESERVATION] > 0)
        <li>
            <a href="{{URL::action('Admin\UserCreditController@getNotification')}}?event_code={{C::AN_USER_CREDIT_INCREASE_RESERVATION}}">
                <div>
                    <i class="fa fa-comment fa-fw"></i>{{fa($notificationsCount['users'][C::AN_USER_CREDIT_INCREASE_RESERVATION])}} {{trans('general-words.user_credit_event_'.C::UC_E_INCREASE_USER_RESERVATION)}}
                    <span class="pull-left text-muted small">مشاهده</span>
                </div>
            </a>
        </li>
        <li class="divider"></li>
        @endif
    </ul>
</li>
@endif

@if (array_sum($notificationsCount['orgs']) > 0)
<li class="dropdown notification" title="سازمان‌ها">
    <a class="dropdown-toggle text-danger" data-toggle="dropdown" href="#">
        <i class="fa fa-building fa-fw"></i>  <i class="fa fa-caret-down"></i>
    </a>
    <ul class="dropdown-menu dropdown-alerts">
        @if ($notificationsCount['orgs'][C::AN_ORG_CREDIT_INCREASE_PAYMENT] > 0)
        <li>
            <a href="{{URL::action('Admin\OrganizationCreditController@getNotification')}}?event_code={{C::AN_ORG_CREDIT_INCREASE_PAYMENT}}">
                <div>
                    <i class="fa fa-comment fa-fw"></i>{{fa($notificationsCount['orgs'][C::AN_ORG_CREDIT_INCREASE_PAYMENT])}} {{trans('general-words.organization_credit_event_'.C::OC_E_INCREASE_ORG_PAYMENT)}}
                    <span class="pull-left text-muted small">مشاهده</span>
                </div>
            </a>
        </li>
        <li class="divider"></li>
        @endif
    </ul>
</li>
@endif

@if (array_sum($notificationsCount['newsletter']) > 0)
<li class="dropdown notification" title="خبرنامه">
    <a class="dropdown-toggle text-danger" data-toggle="dropdown" href="#">
        <i class="fa fa-envelope-square fa-fw"></i>  <i class="fa fa-caret-down"></i>
    </a>
    <ul class="dropdown-menu dropdown-alerts">
        @if ($notificationsCount['newsletter'][C::AN_NEW_NEWSLETTER_USER] > 0)
        <li>
            <a href="{{URL::action('Admin\Newsletter\UserController@getNotification')}}">
                <div>
                    <i class="fa fa-comment fa-fw"></i>{{fa($notificationsCount['newsletter'][C::AN_NEW_NEWSLETTER_USER])}} کاربر خبرنامه جدید
                    <span class="pull-left text-muted small">مشاهده</span>
                </div>
            </a>
        </li>
        <li class="divider"></li>
        @endif
    </ul>
</li>
@endif

@if (array_sum($exdPendingReservations) > 0)
<li class="dropdown notification" title="خبرنامه">
    <a class="dropdown-toggle text-warning" data-toggle="dropdown" href="#">
        <i class="fa fa-exclamation-circle fa-fw"></i>  <i class="fa fa-caret-down"></i>
    </a>
    <ul class="dropdown-menu dropdown-user">
        @if ($exdPendingReservations[C::NOT_ENTERED] > 0)
        <li>
            <a href="{{URL::action('Admin\ReservationController@getPeriod')}}?filter=not_entered_expired&entered={{C::NOT_ENTERED}}">
                <i class="fa fa-sign-in fa-fw"></i> {{fa($exdPendingReservations[C::NOT_ENTERED])}} رزرو وارد نشده منقضی
            </a>
        </li>
        <li class="divider"></li>
        @endif
        @if ($exdPendingReservations[C::PENDING] > 0)
        <li>
            <a href="{{URL::action('Admin\ReservationController@getPeriod')}}?entered={{C::PENDING}}">
                <i class="fa fa fa-spinner fa-fw"></i> {{fa($exdPendingReservations[C::PENDING])}} رزرو در حال بررسی
            </a>
        </li>
        <li class="divider"></li>
        @endif
    </ul>
</li>
@endif

<!DOCTYPE html>
<html lang="fa" ng-app="Poolticket">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="MagnaIT">

    <title>@yield('title')</title>

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link rel="apple-touch-icon" sizes="57x57" href="/admin-assets/dist/images/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/admin-assets/dist/images/favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/admin-assets/dist/images/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/admin-assets/dist/images/favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/admin-assets/dist/images/favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/admin-assets/dist/images/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/admin-assets/dist/images/favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/admin-assets/dist/images/favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/admin-assets/dist/images/favicons/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/admin-assets/dist/images/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/admin-assets/dist/images/favicons/favicon-194x194.png" sizes="194x194">
    <link rel="icon" type="image/png" href="/admin-assets/dist/images/favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/admin-assets/dist/images/favicons/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/admin-assets/dist/images/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/admin-assets/dist/images/favicons/manifest.json">
    <link rel="mask-icon" href="/admin-assets/dist/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="/admin-assets/dist/images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="/admin-assets/dist/images/favicons/mstile-144x144.png">
    <meta name="msapplication-config" content="/admin-assets/dist/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <link href="{{asset('admin-assets/dist/css/jquery-ui.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin-assets/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin-assets/dist/css/metisMenu.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin-assets/dist/datatables/datatables.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin-assets/dist/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin-assets/dist/css/vex.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin-assets/dist/css/vex-theme-default.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin-assets/dist/css/multi-select.css')}}" rel="stylesheet">
    <link href="{{asset('admin-assets/dist/css/begard.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin-assets/datepicker/jquery.Bootstrap-PersianDateTimePicker.css')}}" rel="styleselect">
    <link href="{{asset('admin-assets/dist/css/main.min.css')}}" rel="stylesheet">

    <script src="{{asset('admin-assets/dist/js/jquery.min.js')}}"></script>
    <script src="{{asset('admin-assets/dist/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('admin-assets/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('admin-assets/dist/js/metisMenu.min.js')}}"></script>
    <script src="{{asset('admin-assets/dist/js/icheck.min.js')}}"></script>
    <script src="{{asset('admin-assets/dist/js/vex.combined.min.js')}}"></script>
    <script src="{{asset('admin-assets/dist/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('admin-assets/dist/js/bootstrap-notify.min.js')}}"></script>
    <script src="{{asset('admin-assets/dist/js/begard.min.js')}}"></script>
    <script src="{{asset('admin-assets/dist/js/underscore-min.js')}}"></script>
    <script src="{{asset('admin-assets/dist/js/jquery.multi-select.js')}}"></script>
    <script src="{{asset('admin-assets/dist/js/parsley.min.js')}}"></script>
    <script src="{{asset('admin-assets/dist/js/parsley-i18n.fa.min.js')}}"></script>
    <script src="{{asset('admin-assets/dist/js/jquery.inputmask.bundle.min.js')}}"></script>
    <script src="{{asset('admin-assets/datepicker/calendar.js')}}"></script>
    <script src="{{asset('admin-assets/datepicker/jquery.Bootstrap-PersianDateTimePicker.js')}}"></script>
    <script src="{{asset('admin-assets/dist/js/main.min.js')}}"></script>
    <script src="{{asset('admin-assets/dist/js/main-admin.min.js')}}"></script>

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
        window.INDEX_URL = "{{url('/')}}";
        window.BASE_URL = "{{route('admin')}}";
        window.MSGS = {
            confirm: "{{Lang::get('admin-messages.delete-confirm')}}",
            eanu: "{{Lang::get('admin-messages.eanu')}}",
            updated: "{{Lang::get('admin-messages.update_successfully')}}"
        }
    </script>
    @yield('header')
</head>
<body>

    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            @include('admin.layout.header')
            @include('admin.layout.sidebar')
        </nav>
        <div id="page-wrapper">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
    </div>

    @yield('footer')
</body>
</html>

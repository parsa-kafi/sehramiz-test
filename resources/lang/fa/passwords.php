<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'token' => 'متاسفانه کد تایید تغییر رمز عبور درست نیست و یا تاریخ انقضا آن گذشته است.',
    'reset' => 'Your password has been reset!',
    'front-sent' => 'لینک بازیابی رمز عبور، به ایمیل شما ارسال شد.',
    'front-user' => "ما هیچ کاربری با این ایمیل پیدا نکردیم.",
    'front-sms-sent' => 'پیامی حاوی کد تغییر رمز به شما ارسال شد.',
    'user' => 'ما کاربری با این مشخصات نمی‌توانیم پیدا کنیم.',
    'sms-not-sent' => 'متاسفانه خطایی در زمان ارسال پیام به شماره همراه شما بوجود آمده است. لطفا دوباره امتحان نمایید.'

];
